<?php

include_once 'init/initApp.php';
include_once 'init/ConnexionDb.php';
include_once 'model/HomeModel.class.php';


$vehicleTypes=[];

try {
    $model = new HomeModel();
    $errors =[];

    $vehicleTypes = $model->getVehiclesTypes ($pdo);
}

catch (DomainException $exception)
{

    array_push($errors, $exception->getMessage());
    // Réaffichage du formulaire avec un message d'erreur.
    // Sélection et affichage du template PHTML.
    $template = 'message';
    include PATH_PAGES.'layout.phtml';

}

// Sélection et affichage du template PHTML.
$template = 'vehicles';
include PATH_PAGES.'layout.phtml';
