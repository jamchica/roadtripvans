<?php

include_once 'init/initApp.php';
include_once 'init/ConnexionDb.php';
include_once 'model/HomeModel.class.php';




try {
    $model = new HomeModel();
    $errors =[];

    $agencies = $model->getAgencies ($pdo);
}

catch (DomainException $exception)
{

    array_push($errors, $exception->getMessage());
    // Réaffichage du formulaire avec un message d'erreur.
    // Sélection et affichage du template PHTML.
    $template = 'message';
    include PATH_PAGES.'layout.phtml';

}

// Sélection et affichage du template PHTML.
$template = 'agencies';
include PATH_PAGES.'layout.phtml';
