<?php

class BookingModel {


    function getVehiclesAvailable ($pdo, $agencyId, $vehicleType) {
        

            $sql = "SELECT ID, NAME, NB_PERSON FROM `VEHICLE_TYPE`";
            $resultSet = $pdo->query($sql);
            $vehicles = $resultSet->fetchAll();
            
            if(empty($vehicles) == true)
            {
                throw new DomainException
                (
                    "Types de vehicules indéfinis"
                );
            }
        return $vehicles;
    }


    function getVehicleAvailableOnPeriod ($pdo, $agencyId, $vehicleType, $userBeginDate, $userEndDate) {
        
        //Lister tous les véhicules de ce type de l'agence
        //pour chaque venicule, recuperer toutes les périodes dont :
         //le dateFin user > dateDebutResa AND
         //date debutUser > date fin resa
        //si collelction vide alors retourner le véhicule

        $sql = "SELECT ID, YEAR, FUEL, GEAR FROM `VEHICLE` WHERE BASE_CAR_ID=:typeVehicule
        AND AGENCY=:agencyId";
        
        //echo $vehicleType , PHP_EOL , $agencyId, PHP_EOL ;

        $resultSet = $pdo->prepare($sql);
        $resultSet->execute(['typeVehicule' => $vehicleType,
            'agencyId' => $agencyId]);
        $vehicles = $resultSet->fetchAll();
        
        if(empty($vehicles) == true)
        {
            throw new DomainException
            (
                "Pas de véhicules de ce type pour cette agence. Veuillez modifier votre demande."
            );
        }


        $sql = "SELECT BOOKING.ID, DATE_CREATION, VEHICLE_ID, BOOK_PERIOD.ID, DATE_BEGIN, DATE_END FROM `BOOK_PERIOD` 
            INNER JOIN `BOOKING` ON BOOKING.PERIOD_ID = BOOK_PERIOD.ID
            WHERE VEHICLE_ID=:vehicule";

        $availableVehiculeId = 0;
        
        foreach ($vehicles as $vehicle) {
            $vehiculeId = $vehicle['ID'];
            $isAvailable=true;
            $resultSet = $pdo->prepare($sql);
            $resultSet->execute(['vehicule' => $vehiculeId]);
            $bookedPeriods = $resultSet->fetchAll();

            
            foreach($bookedPeriods as $bookedPeriod) {
                if ($userEndDate > $bookedPeriod['DATE_BEGIN'] 
                    && $userBeginDate < $bookedPeriod['DATE_END'])
                    {
                        //vehicule déja réservé sur la période demandée
                        $isAvailable=false;
                        break;

                    }
            }

            if ($isAvailable==true) {
                $availableVehiculeId = $vehiculeId;
                break;
            }

        }

        if ($isAvailable==false) {
            {
                throw new DomainException ("Aucun véhicule de ce type disponible sur la période à cette agence. 
                Veuillez modifier votre demande.");
            }
        }


    return $availableVehiculeId;
}


    function createAddress ($pdo, 
        $addressLine1,
        $addressLine2,
        $zipCode,
        $city) {

            $country='France'; //pour 1ere version

            $sql = "INSERT INTO 
                `ADRESS` ( `LINE_1`, `LINE_2`, `ZIP_CODE`, `CITY`, `COUNTRY`) 
                VALUES (:addressLine1, 
                :addressLine2, 
                :zipCode, 
                :city, 
                :country)";

            $result = $pdo->prepare($sql);
            $result->execute(['addressLine1' => $addressLine1,
                'addressLine2' => $addressLine2,
                'zipCode' => $zipCode,
                'city'=> $city, 
                'country' => $country]);

            $addressId  = $pdo->lastInsertId(); //marche avant commit 

            if($addressId==0)
            {
                throw new DomainException ("Réservation incomplète. Erreur lors de l'enregistrement de l'adresse");
            }

            return $addressId;
            
    }

    function createClient ($pdo, 
        $name,
        $firstName,
        $addressId,
        $phone,
        $mail) {

       
        $sql = "INSERT INTO `CLIENT` (`NAME`, `FIRSTNAME`, `ADDRESS_ID`, EMAIL, TELEPHONE) 
            VALUES (
                :name, 
                :firstName, 
                :addressId,
                :telephone, 
                :mail)";

        $result = $pdo->prepare($sql);
        $clientId = $result->execute(['name' => $name,
            'firstName' => $firstName,
            'addressId' => $addressId,
            'telephone'=> $phone, 
            'mail' => $mail]);

        $clientId  = $pdo->lastInsertId(); //marche avant commit 
        if($clientId==0)
        {
            throw new DomainException ("Réservation incomplète. Erreur lors de l'enregistrement du client");
        }

        return $clientId;

    }

    function createPeriod ($pdo, 
        $beginDate,
        $endDate) {


        $sql = "INSERT INTO `BOOK_PERIOD` (`DATE_BEGIN`, `DATE_END`) 
                VALUES (:beginDate, :endDate)";


        $result = $pdo->prepare($sql);
        $periodId = $result->execute(['beginDate' => $beginDate,
            'endDate' => $endDate]);
        $periodId = $pdo->lastInsertId(); //marche avant commit    


        if($periodId==0)
        {
            throw new DomainException ("Réservation incomplète. Erreur lors de l'enregistrement de la période");
        }
        return $periodId;

    }


    function createBooking ($pdo, $vehicleId, $periodId, $clientId) {
              // ID	DATE_CREATION	VEHICLE_ID	PERIOD_ID
        $sql = "INSERT INTO `BOOKING` (`DATE_CREATION`, `VEHICLE_ID`, `PERIOD_ID`, `CLIENT_ID`) 
              VALUES (NOW(), :vehicleId, :periodId, :clientId)";

        $result = $pdo->prepare($sql);
        $result->execute(['vehicleId' => $vehicleId,
            'periodId' => $periodId, 
            'clientId' => $clientId]);

        $bookingId = $pdo->lastInsertId(); //marche avant commit    


        if($bookingId==0)
        {
            throw new DomainException ("Réservation incomplète. Erreur lors de l'enregistrement de la réservation");
        }

        return $bookingId;

    }

    
}

