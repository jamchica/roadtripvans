-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  Dim 04 fév. 2018 à 19:42
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `vanbook`
--

-- --------------------------------------------------------

--
-- Structure de la table `ADRESS`
--

CREATE TABLE `ADRESS` (
  `ID` int(11) NOT NULL,
  `LINE_1` varchar(80) NOT NULL DEFAULT '',
  `LINE_2` varchar(80) DEFAULT '',
  `ZIP_CODE` varchar(10) NOT NULL,
  `CITY` varchar(80) DEFAULT '',
  `COUNTRY` varchar(80) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `ADRESS`
--

INSERT INTO `ADRESS` (`ID`, `LINE_1`, `LINE_2`, `ZIP_CODE`, `CITY`, `COUNTRY`) VALUES
(1, '5 Rue Bayard', '', '44100', 'Nantes', 'France'),
(2, '85 Rue Ginguené', '', '35000', 'Rennes', 'France'),
(3, '20 bis rue Albert Priolet', '', '78100', 'Saint-Germain-en-Laye', 'France'),
(4, '3 rue des Lilas', '', '37000', 'Tours', 'France'),
(5, '3 rue du Bayou', 'Louisianne', '12345', 'Nouvelle-Orléans', 'USA'),
(6, '2 rue des LiLas', 'Etage 3', '23456', 'Brest', 'France'),
(7, '2 rue des LiLas', 'Etage 3', '23456', 'Brest', 'France'),
(8, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(9, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(10, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(11, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(12, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(13, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(14, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(15, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(16, '84 rue des Pyrennées', 'Etage 3', '23456', 'Brest', 'France'),
(17, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(18, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(19, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(20, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(21, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(22, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(23, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(24, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(25, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(26, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(27, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(28, '2 rue des LiLas', 'Etage 6', '23456', 'Brest', 'France'),
(29, '2 rue des LiLas', 'Etage 8', '23456', 'Brest', 'France'),
(30, '2 rue des LiLas', 'Etage 8', '23456', 'Brest', 'France'),
(31, '2 rue des LiLas', 'Etage 9', '23456', 'Brest', 'France'),
(32, '2 rue des LiLas', 'Etage 13', '23456', 'Brest', 'France'),
(33, '2 rue des LiLas', 'Etage 3', '23456', 'Brest', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `AGENCY`
--

CREATE TABLE `AGENCY` (
  `ID` int(11) NOT NULL,
  `ZONE` varchar(50) NOT NULL,
  `ADRESS_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `AGENCY`
--

INSERT INTO `AGENCY` (`ID`, `ZONE`, `ADRESS_ID`) VALUES
(4, 'Nantes', 1),
(5, 'Rennes', 2),
(6, 'IDF Ouest', 3);

-- --------------------------------------------------------

--
-- Structure de la table `BOOKING`
--

CREATE TABLE `BOOKING` (
  `ID` int(11) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `VEHICLE_ID` int(11) NOT NULL,
  `PERIOD_ID` int(10) NOT NULL,
  `CLIENT_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `BOOKING`
--

INSERT INTO `BOOKING` (`ID`, `DATE_CREATION`, `VEHICLE_ID`, `PERIOD_ID`, `CLIENT_ID`) VALUES
(2, '0000-00-00 00:00:00', 2, 4, 10),
(3, '0000-00-00 00:00:00', 2, 5, 11),
(4, '2018-01-31 14:52:40', 2, 6, 12),
(5, '2018-01-31 15:27:18', 2, 7, 13),
(6, '2018-01-31 15:30:41', 2, 8, 14),
(7, '2018-01-31 15:40:52', 2, 9, 15),
(8, '2018-01-31 15:41:38', 2, 10, 16),
(9, '2018-02-01 12:32:47', 2, 11, 17);

-- --------------------------------------------------------

--
-- Structure de la table `BOOK_PERIOD`
--

CREATE TABLE `BOOK_PERIOD` (
  `DATE_BEGIN` datetime NOT NULL,
  `DATE_END` datetime NOT NULL,
  `ID` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `BOOK_PERIOD`
--

INSERT INTO `BOOK_PERIOD` (`DATE_BEGIN`, `DATE_END`, `ID`) VALUES
('2018-01-25 00:00:00', '2018-01-26 00:00:00', 1),
('2018-01-25 00:00:00', '2018-01-30 00:00:00', 2),
('0000-00-00 00:00:00', '0000-00-00 00:00:00', 3),
('0000-00-00 00:00:00', '0000-00-00 00:00:00', 4),
('0000-00-00 00:00:00', '0000-00-00 00:00:00', 5),
('0000-00-00 00:00:00', '0000-00-00 00:00:00', 6),
('0000-00-00 00:00:00', '0000-00-00 00:00:00', 7),
('0000-00-00 00:00:00', '0000-00-00 00:00:00', 8),
('0000-00-00 00:00:00', '0000-00-00 00:00:00', 9),
('0000-00-00 00:00:00', '0000-00-00 00:00:00', 10),
('1970-01-01 01:00:01', '0000-00-00 00:00:00', 11),
('0000-00-00 00:00:00', '1970-01-01 01:00:03', 12),
('0000-00-00 00:00:00', '1970-01-01 01:00:03', 13),
('2018-01-02 13:10:00', '2018-03-02 13:10:00', 14),
('2018-06-02 14:08:00', '2018-10-02 14:08:00', 15);

-- --------------------------------------------------------

--
-- Structure de la table `CLIENT`
--

CREATE TABLE `CLIENT` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL DEFAULT '',
  `FIRSTNAME` varchar(50) NOT NULL DEFAULT '',
  `ADDRESS_ID` int(11) NOT NULL,
  `EMAIL` varchar(30) DEFAULT NULL,
  `TELEPHONE` varchar(20) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `CLIENT`
--

INSERT INTO `CLIENT` (`ID`, `NAME`, `FIRSTNAME`, `ADDRESS_ID`, `EMAIL`, `TELEPHONE`) VALUES
(1, 'Jackson', 'Thelma', 4, NULL, ''),
(2, 'Barber', 'Louise', 4, NULL, ''),
(7, 'Parker', 'Bonnie', 5, NULL, ''),
(8, 'Barrow', 'Clyde', 5, NULL, ''),
(9, 'Dupont', 'Virgile', 21, '123456789', 'vdupont@toto.titi'),
(10, 'Dupont', 'Virgile', 22, '123456789', 'vdupont@toto.titi'),
(11, 'Dupont', 'Virgile', 23, '123456789', 'vdupont@toto.titi'),
(12, 'Dupont', 'Virgile', 24, '123456789', 'vdupont@toto.titi'),
(13, 'Dupont', 'Virgile', 25, '123456789', 'vdupont@toto.titi'),
(14, 'Dupont', 'Virgile', 26, '123456789', 'vdupont@toto.titi'),
(15, 'Dupont', 'Virgile', 27, '123456789', 'vdupont@toto.titi'),
(16, 'Dupont', 'Virgile', 28, '123456789', 'vdupont@toto.titi'),
(17, 'Dupont', 'Virgile', 29, '123456789', 'vdupont@toto.titi'),
(18, 'Dupont', 'Virgile', 30, '123456789', 'vdupont@toto.titi'),
(19, 'Dupont', 'Virgile', 31, '123456789', 'vdupont@toto.titi'),
(20, 'Dupont', 'Virgile', 32, '123456789', 'vdupont@toto.titi'),
(21, 'Verlaine', 'Paul', 33, '123456789', 'verlaine@toto.titi');

-- --------------------------------------------------------

--
-- Structure de la table `CONFORT_PACK`
--

CREATE TABLE `CONFORT_PACK` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL DEFAULT '',
  `DESCRIPTION` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `VEHICLE`
--

CREATE TABLE `VEHICLE` (
  `ID` int(11) NOT NULL,
  `BASE_CAR_ID` int(11) NOT NULL,
  `YEAR` int(4) NOT NULL,
  `FUEL` varchar(10) NOT NULL DEFAULT '',
  `GEAR` varchar(15) NOT NULL DEFAULT '',
  `AGENCY` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `VEHICLE`
--

INSERT INTO `VEHICLE` (`ID`, `BASE_CAR_ID`, `YEAR`, `FUEL`, `GEAR`, `AGENCY`) VALUES
(2, 3, 2000, 'Essence', 'Manuel', 4),
(3, 5, 2016, 'Essence', 'Manuel', 4),
(4, 1, 2015, 'Diesel', 'Manuel', 4),
(5, 3, 2016, 'Essence', 'Manuel', 6),
(6, 4, 2015, 'Essence', 'Automatique', 6),
(7, 2, 2017, 'Diesel', 'Manuel', 5),
(8, 1, 2015, 'Diesel', 'Manuel', 5),
(9, 5, 2016, 'Essence', 'Manuel', 5);

-- --------------------------------------------------------

--
-- Structure de la table `VEHICLE_TYPE`
--

CREATE TABLE `VEHICLE_TYPE` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(40) NOT NULL DEFAULT '',
  `NB_PERSON` smallint(2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `VEHICLE_TYPE`
--

INSERT INTO `VEHICLE_TYPE` (`ID`, `NAME`, `NB_PERSON`) VALUES
(1, 'Nissan NV200', 4),
(2, 'Fiat Ducato', 6),
(3, 'VW Combi', 4),
(4, 'Coccinelle', 1),
(5, 'VW Caddy Camper', 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ADRESS`
--
ALTER TABLE `ADRESS`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Index pour la table `AGENCY`
--
ALTER TABLE `AGENCY`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `FK_ADRESS` (`ADRESS_ID`);

--
-- Index pour la table `BOOKING`
--
ALTER TABLE `BOOKING`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_CLIENT` (`CLIENT_ID`),
  ADD KEY `FK_PERIOD` (`PERIOD_ID`),
  ADD KEY `FK_VEHICLE` (`VEHICLE_ID`);

--
-- Index pour la table `BOOK_PERIOD`
--
ALTER TABLE `BOOK_PERIOD`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`);

--
-- Index pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Index pour la table `CONFORT_PACK`
--
ALTER TABLE `CONFORT_PACK`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Index pour la table `VEHICLE`
--
ALTER TABLE `VEHICLE`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `FK_VEHICLE_TYPE` (`BASE_CAR_ID`),
  ADD KEY `FK_AGENCY` (`AGENCY`);

--
-- Index pour la table `VEHICLE_TYPE`
--
ALTER TABLE `VEHICLE_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ADRESS`
--
ALTER TABLE `ADRESS`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `AGENCY`
--
ALTER TABLE `AGENCY`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `BOOKING`
--
ALTER TABLE `BOOKING`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `BOOK_PERIOD`
--
ALTER TABLE `BOOK_PERIOD`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `CONFORT_PACK`
--
ALTER TABLE `CONFORT_PACK`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `VEHICLE`
--
ALTER TABLE `VEHICLE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `VEHICLE_TYPE`
--
ALTER TABLE `VEHICLE_TYPE`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `AGENCY`
--
ALTER TABLE `AGENCY`
  ADD CONSTRAINT `FK_ADRESS` FOREIGN KEY (`ADRESS_ID`) REFERENCES `ADRESS` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `BOOKING`
--
ALTER TABLE `BOOKING`
  ADD CONSTRAINT `FK_CLIENT` FOREIGN KEY (`CLIENT_ID`) REFERENCES `CLIENT` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_PERIOD` FOREIGN KEY (`PERIOD_ID`) REFERENCES `BOOK_PERIOD` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_VEHICLE` FOREIGN KEY (`VEHICLE_ID`) REFERENCES `VEHICLE` (`ID`);

--
-- Contraintes pour la table `VEHICLE`
--
ALTER TABLE `VEHICLE`
  ADD CONSTRAINT `FK_AGENCY` FOREIGN KEY (`AGENCY`) REFERENCES `AGENCY` (`ID`),
  ADD CONSTRAINT `FK_VEHICLE_TYPE` FOREIGN KEY (`BASE_CAR_ID`) REFERENCES `VEHICLE_TYPE` (`ID`);
