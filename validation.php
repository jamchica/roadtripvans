<?php

include_once 'init/initApp.php';
include_once 'init/ConnexionDb.php';
include_once 'model/BookingModel.class.php';

/*Validation Enregistrement et message */

// var_dump($_POST);
$errors = [];
$messages = [];

try {


    $model = new BookingModel();

    //$formatFr = "d/m/Y H:i:s";
    //format:'d/m/Y H:i'
    $startingDateFr = date(substr($_POST["startingDate"], 0, 10));
    $endDateFr = date(substr($_POST["endDate"], 0, 10));


    $startingTime = substr($_POST["startingDate"], 11);
    $endTime = substr($_POST["endDate"], 11);
        
    //var_dump($startingDateFr);
    //var_dump($startingTime);



    //$startDateObj = new DateTime($startingDateFr.' '.$startingTime);

    $formFormat = "d/m/Y H:i";
    $startDateObj = DateTime::createFromFormat($formFormat, $startingDateFr.' '.$startingTime);

    //$endDateObj = new DateTime($endDateFr.' '.$endTime);
    $endDateObj = DateTime::createFromFormat($formFormat,$endDateFr.' '.$endTime);
        

        
    //conversion du format pour compatibilité MySQL
    $formatUsa = "Y/m/d H:i:s";

    $startingDateDb = $startDateObj-> format($formatUsa );
    $endDateDb = $endDateObj-> format($formatUsa );
        
		

     // recuperer un vehicule du bon type sur la periode pour cette agence
    $vehiculeId = "test_car";   
    $vehiculeId =  $model->getVehicleAvailableOnPeriod ($pdo, 
        $_POST["shop"], 
        $_POST["vehicule"], //type de vehicule
        $startingDateFr,
        $endDateFr);
      
       
    
    $adresseId = $model->createAddress ($pdo, 
        $_POST["addressLine1"],
        $_POST["addressLine2"],
        $_POST["zipCode"],
        $_POST["city"]);

       

    $clientId = $model->createClient ($pdo, 
        $_POST["name"],
        $_POST["firstName"],
        $adresseId,
        $_POST["phone"],
        $_POST["mail"]);

        
        //echo "clientId=", $clientId , PHP_EOL ;
        

    $periodId = $model->createPeriod ($pdo, 
        $startingDateDb,
        $endDateDb);

    //    var_dump($periodId);


     //echo "periodId=", $periodId , PHP_EOL ;

    $bookingId = $model->createBooking ($pdo, 
        $vehiculeId , 
        $periodId, 
        $clientId); 
    
    //var_dump($bookingId);

 

    array_push($messages, "Votre réservation est enregistrée.");

 
}

catch (DomainException $exception)
{

    //simplifié, en principe il faudrait pouvoir faire un rollback
    var_dump($exception);
   
    array_push($errors, $exception->getMessage());
   
}



 $template = 'message';
 include PATH_PAGES.'layout.phtml';
  







