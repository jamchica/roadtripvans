<?php

include_once 'init/initApp.php';
include_once 'init/ConnexionDb.php';
include_once 'model/HomeModel.class.php';

$errors = [];

/* Récupération des données saises dans le formulaire de réservation  */


/* verifier existence et validité par rapport au périodes de réservation */

if(!array_key_exists('startingDate', $_POST) 
    OR !array_key_exists('endDate', $_POST)
    OR !array_key_exists('vehicle', $_POST)
    OR !array_key_exists('shop', $_POST) )
{
    /* rajouter message  */
    header('Location: ../index.php');
    exit();
}

$userBooking = array(
  "userStartingDate" => $_POST['startingDate'],
  "userEndDate" => $_POST['endDate'],
  "userVehicle" => $_POST['vehicle'],
  "userShop" => $_POST['shop']
);

try {
    $model = new HomeModel();
    $vehicles = $model->getVehiclesTypes ($pdo);
    $agencies = $model->getAgencies ($pdo);
}



catch (DataException $exception)
{
    //TODO
    echo  $exception->getMessage();

    // Réaffichage du formulaire avec un message d'erreur.
    //array_push($errors, $exception->getMessage());
 
}



// if (!isset($_SESSION['count'])) {
//   $_SESSION['count'] = 0;
// } else {
//   $_SESSION['count']++;
// }

$template = 'booking';
include PATH_PAGES.'layout.phtml';