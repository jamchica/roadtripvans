<?php

$user = 'root';
$password = 'root';
$db = 'vanbook';
$host = '127.0.0.1';
$port = 8889;

$dsn = 'mysql:host='.$host.';port='.$port.';dbname='.$db;


$pdo = new PDO
(
    $dsn ,
    $user,
    $password,
    [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ] 
);

