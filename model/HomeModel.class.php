<?php

class HomeModel {


    function getVehiclesTypes ($pdo) {

            $sql = "SELECT ID, NAME, NB_PERSON, URL_IMAGE, DESCRIPTION FROM `VEHICLE_TYPE`";
            $resultSet = $pdo->query($sql);
            $vehicles = $resultSet->fetchAll();

            if(empty($vehicles) == true)
            {
                throw new DomainException
                (
                    "Types de vehicules indéfinis"
                );
            }
        return $vehicles;
    }

    function getAgencies ($pdo) {

        $sql = "SELECT AGENCY.ID, ZONE, PHONE, LINE_1, LINE_2, ZIP_CODE, CITY, COUNTRY FROM `AGENCY`
        INNER JOIN ADRESS ON ADRESS_ID = ADRESS.ID     ";
        $resultSet = $pdo->query($sql);
        $agencies = $resultSet->fetchAll();

        if(empty($agencies) == true)
        {
            throw new DomainException
            (
                "Liste d'agences indéfinie"
            );
        }
        return $agencies;
    }

    function getAgencyVehicleTypes ($pdo, $agencyId) {

        $sql = "SELECT VEHICLE.ID,YEAR,
        FUEL,
        GEAR,
        VEHICLE_TYPE.NAME,
        VEHICLE_TYPE.NB_PERSON
        FROM VEHICLE
        INNER JOIN AGENCY ON AGENCY.ID = VEHICLE.AGENCY
        INNER JOIN VEHICLE_TYPE ON VEHICLE_TYPE.ID = BASE_CAR_ID
        WHERE AGENCY.ID = :agencyId";

        $resultSet = $pdo->prepare($sql);
        $resultSet->execute(['agencyId' => $agencyId]);
        $vehicleTypes = $resultSet->fetchAll();

        if(empty($vehicleTypes))
        {
            throw new DomainException ("Liste de vehicule indéfinie pour cette agence");
        }
        return $vehicleTypes;

    }





}
