<?php

include_once 'init/initApp.php';

/*Default dispatcher for static pages */

$target = $_GET['to'];

if (isset($target) && $target === "legal") {

  $template = 'credits-photo';
}

else if (isset($target) && $target === "faq") {

  $template = 'faq';
}

else if (isset($target) && $target === "cgl") {

  $template = 'cgl';
}

else {
  header('Location: ./index.php');
  exit();
}

include PATH_PAGES.'layout.phtml';
