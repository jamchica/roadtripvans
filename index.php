<?php

include_once 'init/initApp.php';
include_once 'init/ConnexionDb.php';
include_once 'model/HomeModel.class.php';


$errors = [];

try {
   
    $model = new HomeModel();
    $agencies = $model->getAgencies ($pdo);
    /*Initialisation de la liste avec les vehicules de la premiere agence */
    $vehicles = $model->getAgencyVehicleTypes  ($pdo, $agencies[0]['ID']);
    
}

catch (DomainException $exception)
{
   
    array_push($errors, $exception->getMessage());
   
}

// Sélection et affichage du template PHTML.
$template = 'home';
include PATH_PAGES.'layout.phtml';